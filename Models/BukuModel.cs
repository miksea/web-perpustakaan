﻿namespace webperpustakaan.Models
{
    public class BukuModel
    {
        public int Id_buku { get; set; }
        public string Nama_buku { get; set; }
        public DateOnly Tgl_rilis { get; set; }
        public int Id_pengarang { get; set; }
        public int Harga_buku { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Updated_at { get; set; }
    }
}

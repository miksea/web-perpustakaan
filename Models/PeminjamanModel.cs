﻿namespace webperpustakaan.Models
{
    public class PeminjamanModel
    {
        public int Id_peminjaman { get; set; }
        public int Id_user { get; set; }
        public int Id_buku { get; set; }
        public DateTime Tgl_pinjam {  get; set; }
        public DateTime Tgl_Kembali { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Updated_at { get;set; }
    }
}

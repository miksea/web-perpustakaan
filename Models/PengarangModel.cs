﻿namespace webperpustakaan.Models
{
    public class PengarangModel
    {
        public int Id_pengarang { get; set; }
        public string Nama_pengarang { get; set; }
        public DateTime Created_at { get; set; } = DateTime.Now;
        public DateTime Updated_at { get; set; }
    }
}

﻿namespace webperpustakaan.Models
{
    public class PengembalianModel
    {
        public int Id_pengembalian { get; set; }
        public int Id_user { get; set; }
        public int Id_buku { get; set; }
        public DateTime Tgl_kembali { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Updated_at { get; set; }
    }
}
